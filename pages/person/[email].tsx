import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, DatePicker, Form, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const [isEditing, setIsEditing] = useState(false);

  // form values
  const [name, setName] = useState('');
  const [gender, setGender] = useState('');
  const [phone, setPhone] = useState('');
  const [birthday, setBirthday] = useState('');

  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const handleEdit = async (event) => {
    event.preventDefault();

    await save({
      ...data,
      name,
      gender,
      phone,
      birthday,
    });

    setIsEditing(false);
    load();
  };

  useEffect(() => {
    load();
  }, []);

  useEffect(() => {
    if (!!data) {
      setName(data.name);
      setGender(data.gender);
      setPhone(data.phone);
      setBirthday(data.birthday);
    }
  }, [data]);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => setIsEditing((prevState) => !prevState)}>
            {isEditing ? 'Cancel' : 'Edit'}
          </Button>,
        ]}
      >
        {data &&
          (isEditing ? (
            <form onSubmit={handleEdit}>
              <Input
                name="name"
                placeholder="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Input
                name="gender"
                placeholder="Gender"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
              />
              <Input
                name="phone"
                placeholder="Phone"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
              <Input
                name="birthday"
                placeholder="Birthday"
                value={birthday}
                onChange={(e) => setBirthday(e.target.value)}
              />
              <Button htmlType="submit">Save</Button>
            </form>
          ) : (
            <Descriptions size="small" column={1}>
              <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
              <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
              <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>

              <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
            </Descriptions>
          ))}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
